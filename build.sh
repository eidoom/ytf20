#!/usr/bin/env sh

mkdir -p public/
cp show.css public/
cp -r img public/
pandoc --defaults=show.yml
