# Aim {.unlisted}

## Aim
![](img/aim.svg "five-gluon one-loop amplitude -> number")

::: notes
* Restrict ourselves to the hard scattering process and neglect lower-scale effects
    * PDFs
    * Parton showers
    * Jets
* Differential cross-section for 2->3 gluons at one-loop
:::

# Tree level

##
![](img/tree.svg "five-gluon tree-level amplitude")

::: notes
* 5-gluon tree
* What is this diagram?
:::

## Helicity sum
![](img/hel.svg)

::: notes
* Average over initial and sum over final state helicities
* What's a term in this sum?
:::

## Diagram sum
![](img/ds.svg){.spaced}

::: notes
* Sum over all possible diagrams
* All possible vertex structures and permutations of external legs
* 6g: 220
* 7g: 34300
* Can we do better?
:::

## Colour sum
![](img/col.svg)

::: notes
* QCD amplitudes factorise into colour and kinematic contributions
* Organise sum in colour basis
    * eg. here, trace basis: each term is a trace of a non-cyclical permutation of SU(3) generators
    * gives simple colour algebra
* colour-ordered/partial amplitudes
    * only cyclical ordering of external legs
    * here, 10 diagrams in each partial amplitude
    * how to evaluate a partial amplitude?
:::

## Recursion
![](img/bg.svg)
![](img/bcfw.svg)

::: notes
* Could do usual Feynman rules
* Or can build recursively from lower-multiplicity diagrams
    * reuse common building blocks
* Berends-Giele off-shell recursion
* Britto–Cachazo–Feng–Witten on-shell recursion
:::

## Spinor helicity
![](img/psi.svg)
![](img/mhv.svg)

::: notes
* Want concise analytic expression for amplitude
* For massless particles, use spinor helicity formalism
    * Decompose 4-component Dirac spinor into two 2-component Weyl spinors and represent with angle/square-brackets
* Parke-Taylor amplitude: remarkably simple
    * Lesson: complexity in intermediate stages, not final result
* Extra:
    * p in Lorentz group = identity connected component of O(1,3)
    * spinors in spin group = SL(2, C) = double cover of Lorentz group
    * Dirac = (1/2,0)+(0,1/2)
    * Weyl_1 = (1/2,0), Weyl_2 = (0,1/2)
    * Massless: Dirac equation decomposes to 2x Weyl equations
:::

## Amplitude squared
![](img/mat.svg)

::: notes
* Organise as colour matrix multiplied with partial amplitude vectors
* Partial amplitudes are permutations of an analytic expression
* Colour matrix is global: calculate once
:::

# Higher order corrections

## Next-to-Leading-Order
![](img/nlo.svg "NLO contributions"){.small}

::: notes
* Obtain higher order contributions by adding
    * Loops (virtual)
    * Legs (real)
:::

## Cross-section
![](img/xs.svg "cross-section")

::: notes
* Counting powers of the strong coupling, at NLO get
    * tree interfered with loop
    * real^2
* Problem: 
    * UV & IR divergences in loop
    * IR divergences in real integration (will come back to this)
* Solution: dimensional regularisation "D=4-2*eps"
    * replace infinities with poles in dimensional regulator, ie. 1/eps...
    * Renormalisation fixes UV poles
    * R & V IR poles exactly cancel to give finite NLO cross-section
    * must since observable
    * proved by Bloch–Nordsieck and Kinoshita–Lee–Nauenberg theorems
* But analytic integration is intractable
    * Want numerical Monte-Carlo integration
    * Only works in integer dimensions
:::

# Infrared divergences

## Subtraction scheme
![](img/sub.svg "subtraction scheme")

::: notes
* We add zero
* Two counterterms that regulate each contribution and sum to O(eps) after integration
* Subtlety: counterterms lie in different phase spaces
    * Catani-Seymour dipole subtraction
    * Frixione-Kunszt-Signer method
* How to construct counterterms?
:::

##
![](img/ss1.svg "amplitude landscape")

::: notes
* Consider the real contribution to the NLO amplitude
* It has singularities in infrared limits of phase space...
:::

##
![](img/ss2.svg "amplitude divergences")

::: notes
* ...which we isolate...
:::

##
![](img/ss3.svg "counterterms")

::: notes
* ...to give our counterterm
:::

##
![](img/ss4.svg "regulated amplitude")

::: notes
* Subtracting it gives a regulated function to integrate
:::

##
![](img/coll.svg "collinear splitting function")
![](img/soft.svg "soft eikonal function")

::: notes
* In practice, we obtain these limits via another factorisation property: infrared factorisation
* In infrared divergent regions of phase space, amplitude factorises to lower multiplicity amplitude times infrared function
* Collinear limit: two legs become indistinguishable as their angle of separation goes to zero
    * Splitting function
* Soft limit: energy of leg goes to zero
    * Eikonal function
* These functions are universal: process-independent
    * Use to build general counterterms
:::

# Loops

## Algebraic
![](img/int.svg "integrand reduction")

* Integrand reduction
* Coefficients
    * Generalised unitarity
    * OPP
* Master integrals
    * Integration-by-parts identities
    * Laporta

::: notes
* Loops have two complexities: 1. algebraic (number of terms)
* Integrand reduction: diagram starts as rank 5 tensor integral > a sum of scalar integrals (numerator doesn't have Lorentz indices)
    * Partial fraction integrand numerator to cancel propagators
* Coefficients from generalised unitarity cuts
    * fancy way of saying: replace propagators with delta functions to turn loop into product of on-shell trees
    * evaluate integrand on different cuts and reconstruct by polynomial fit
* Ossola, Pittau, and Papadopoulos
* Linearise set of integrals by differential equation relations
* Rational part
    * from eps/eps
    * generalised unitarity in higher dimensions, ficticious loop momentum mass, Feynman diagrams, string theory
:::

## Analytic
<div class="images">
![](img/li1.svg "logarithm")

![](img/li2.svg "dilogarithm")
</div>

* Polylogarithms
* Laurent expansion

::: notes
* 2. analytic - the types of numbers the integrals give
* More loops: multiple polylogarithms
* Massive loops: elliptic functions
:::

# Computation

## Techniques
* Large intermediate numbers
    * Finite fields
        * Momentum twistor variables
* High performance
    * `FORM`
    * `C++`

::: notes
* Large cancellations in intermediate steps
* Floating point errors
* FF: integer arithmetic modulo a prime field
    * Evaluation over sufficiently many fields allows reconstruction of result
    * cf Chinese Remainder Theorem
* Requires rational expressions
    * Guaranteed by momentum twistor variables
    * Construct from components of a Weyl spinor and the dual of the other
* FORM: operation optimisation
* C++: performance-focussed
:::

# Overview {.unlisted}

## Overview
* Calculation organisation
* Infrared regulation
* Loop reduction
* Computation

![](img/aim.svg "five-gluon one-loop amplitude -> number"){.small}
