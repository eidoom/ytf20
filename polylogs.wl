(* ::Package:: *)

SetDirectory[NotebookDirectory[]];


a=3;
ComplexPlot3D[Log[z],{z,-a-a*I,a+a*I},PlotLegends->Automatic,AxesLabel->{"Re(z)","Im(z)","Log(z)"}]


a=3;
ComplexPlot3D[PolyLog[0,z],{z,-a-a*I,a+a*I},PlotLegends->Automatic,AxesLabel->{"Re(z)","Im(z)",Row[{Subscript["Li","0"],"(z)"}]}]


a=3;
L1=ComplexPlot3D[PolyLog[1,z],{z,-a-a*I,a+a*I},PlotLegends->Automatic,AxesLabel->{"Re(z)","Im(z)",Row[{Subscript["Li","1"],"(z)"}]}]


Export["img/li1.svg",L1];


a=3;
L2=ComplexPlot3D[PolyLog[2,z],{z,-a-a*I,a+a*I},PlotLegends->Automatic,AxesLabel->{"Re(z)","Im(z)",Row[{Subscript["Li","2"],"(z)"}]}]


Export["img/li2.svg",L2];


a=3;
ComplexPlot3D[PolyLog[3,z],{z,-a-a*I,a+a*I},PlotLegends->Automatic,AxesLabel->{"Re(z)","Im(z)",Row[{Subscript["Li","3"],"(z)"}]}]
