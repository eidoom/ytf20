# [ytf20](https://gitlab.com/eidoom/ytf20)

* [Live](https://eidoom.gitlab.io/ytf20)
* [For print](https://eidoom.gitlab.io/ytf20/?print-pdf) ([instructions](https://revealjs.com/pdf-export/#instructions))

## Thanks to
* [pandoc](https://pandoc.org/)
* [reveal.js](https://revealjs.com/)
